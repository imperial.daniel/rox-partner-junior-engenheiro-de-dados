# Introdução
## Engenheiro de Dados: Daniel Imperial

Esta é a documentação para o teste do processo seletivo da ROX Partner, para a posição de Engenheiro de Dados Jr. Aqui, estarão também compilados, nos tópicos respectivos:

- Decisões sobre a arquitetura utilizada;
- Implementação da solução;
- Instruções de execução da solução.

## Conteúdo do repositório

O repositório contém:

- Os arquivos CSV usados para o teste;
- As imagens de alimentação para este repositório;
- O template XML do Apache NiFi;
- Arquivos SQL correspondendo, respectivamente, à geração de tabelas (gerenciada pelo MySQL) e as queries utilizadas na parte de análise de dados (ao final deste documento).

# Arquitetura

A arquitetura pode ser vista conforme a imagem abaixo.

![arquitetura](arquitetura.png)

O projeto confere a utilização de uma imagem `docker` com a solução isolada, de forma a permitir a portabilidade e persistência dos dados. A orquestração dos dados é coordenada através do `Apache NiFi`, inicializada dentro deste *contêiner*. Em paralelo, também roda-se um servidor `mysql` para o armazenamento das informações em CSV, devidamente filtradas utilizando processadores do orquestrador. As informações CSV são extraídas de uma *Cloud Storage* do Google Cloud Services (GCS), utilizando uma conta de serviço especial e acesso em chave JSON para a solução. 

Os contêineres respectivos são:

- `mysql` como solução de armazenamento de dados relacionais;
- `zookeeper` como servidor de configuração para gerenciar o Apache NiFi;
- `apache/nifi` representando a solução de orquestração de dados, realizando as tarefas de extração;
- `apache/nifi-registry` servindo como serviço centralizado de armazenamento dos dados, de forma a manter persistência dos dados.

A solução funciona apenas em `localhost`, de forma a manter a segregação dos dados e mantimento das informações de forma segura e isolada.

# Implementação da Solução

A solução é implementada através da montagem de um Docker Compose, contida neste repositório. A solução inicial foi feita em `docker-compose.yml` por formas de prototipagem, e posteriormente construída.

A imagem abaixo constitui a *pipeline* de dados. `ListGCSBucket` e `FetchGCSObject` são utilizados em conjunto para iterar nos objetos. O database é pré-inicializado através da execução de um arquivo script `init.sql`, populando o banco com os esquemas, tabelas e vinculações de chaves respectivas. Assumindo a consistência dos nomes dos CSVs, é possível utilizar o processador `RouteOnAttribute` para criar uma condicional pelo nome do arquivo. Para cada propriedade, constitui-se um processador único de salvamento no *database* estabelecido.  

# Execução da Solução

Para executar a solução, assume-se a presença de `docker` na máquina anfitriã. Para rodar, clone o repositório e na pasta, execute:

``docker compose up --build``

Acesse no seu navegador a seguinte URL:

``localhost:8091/nifi``

Na página, no canto esquerdo inferior, clique em 'Upload Template' e insira o arquivo 'ROX_Container_Parametrizado.xml'. Depois, clique e arraste o item *Template*, selecionando o template respectivo.
Clique na engrenagem no canto inferior esquerdo da página, e clique na aba 'ControllerServices'. Para os serviços, siga as instruções:

- Em CSVReader, clique no símbolo de raio (Enable).
- Em DBCPConnectionPool, clique na engrenagem, na aba 'Properties', e confira se a URL corresponde ao valor '10.5.0.5' Nos campos 'Database User' e 'Database Password', coloque, respectivamente, 'root' e 'Rotpw2020'. Depois, saia deste menu e clique no botão de raio.

Posteriormente, clique no símbolo de Iniciar para começar o fluxo. Os dados podem ser lidos através de um DBMS que suporte MySQL.

# Respostas Queries

### Query 1

Escreva uma query que retorna a quantidade de linhas na tabela Sales.SalesOrderDetail pelo campo SalesOrderID, desde que tenham pelo menos três linhas de detalhes.

![Query 1](query1.png)

### Query 2

Escreva uma query que ligue as tabelas Sales.SalesOrderDetail, Sales.SpecialOfferProduct e Production.Product e retorne os 3 produtos (Name) mais vendidos (pela soma de OrderQty), agrupados pelo número de dias para manufatura (DaysToManufacture).

![Query 2](query2.png)

### Query 3

Escreva uma query ligando as tabelas Person.Person, Sales.Customer e Sales.SalesOrderHeader de forma a obter uma lista de nomes de clientes e uma contagem de pedidos efetuados.

![Query 3](query3.png)

### Query 4

Escreva uma query usando as tabelas Sales.SalesOrderHeader, Sales.SalesOrderDetail e Production.Product, de forma a obter a soma total de produtos (OrderQty) por ProductID e OrderDate.

![Query 4](query4.png)

### Query 5

Escreva uma query mostrando os campos SalesOrderID, OrderDate e TotalDue da tabela Sales.SalesOrderHeader. Obtenha apenas as linhas onde a ordem tenha sido feita durante o mês de setembro/2011 e o total devido esteja acima de 1.000. Ordene pelo total devido decrescente.

![Query 5](query5.png)
