CREATE SCHEMA IF NOT EXISTS ROX;

CREATE TABLE IF NOT EXISTS ROX.SpecialOfferProduct
(
    SpecialOfferID INT PRIMARY KEY NOT NULL,
    ProductID INT,
    rowguid VARCHAR(45),
    ModifiedDate DATETIME
);

CREATE TABLE IF NOT EXISTS ROX.Product
(
    ProductID INT PRIMARY KEY NOT NULL,
    Name VARCHAR(45),
    ProductNumber VARCHAR(45),
    MakeFlag VARCHAR(45),
    FinishedGoodsFlag INT,
    Color VARCHAR(45),
    SafetyStockLevel VARCHAR(45),
    ReorderPoint INT,
    StandardCost VARCHAR(45),
    ListPrice VARCHAR(45),
    Size VARCHAR(45),
    SizeUnitMeasureCode VARCHAR(45),
    WeightUnitMeasureCode VARCHAR(45),
    Weight VARCHAR(45),
    DaysToManufacture VARCHAR(45),
    ProductLine VARCHAR(45),
    Class VARCHAR(45),
    Style VARCHAR(45),
    ProductSubcategoryID VARCHAR(45),
    ProductModelID VARCHAR(45),
    SellStartDate DATETIME,
    SellEndDate DATETIME,
    DiscontinuedDate DATETIME,
    rowguid VARCHAR(45),
    ModifiedDate DATETIME
);

CREATE TABLE IF NOT EXISTS ROX.Person
(
    BusinessEntityID INT PRIMARY KEY NOT NULL,
    PersonType VARCHAR(45),
    NameStyle INT,
    Title VARCHAR(45),
    FirstName VARCHAR(45),
    MiddleName VARCHAR(45),
    LastName VARCHAR(45),
    SUFFIX VARCHAR(45),
    EmailPromotion INT,
    AdditionalContactInfo VARCHAR(45),
    Demographics VARCHAR(45),
    jrowguid VARCHAR(45),
    ModifiedDate DATETIME
);

CREATE TABLE IF NOT EXISTS ROX.SalesOrderHeader
(
    SalesOrderID INT PRIMARY KEY NOT NULL,
    RevisionNumber INT,
    OrderDate DATETIME,
    DueDate DATETIME,
    ShipDate DATETIME,
    Status INT,
    OnlineOrderFlag INT,
    SalesOrderNumber VARCHAR(45),
    PurchaseOrderNumber VARCHAR(45),
    AccountNumber VARCHAR(45),
    CustomerID INT,
    SalesPersonID FLOAT,
    TerritoryID INT,
    BillToAddressID INT,
    ShipToAddressID INT,
    ShipMethodID INT,
    CreditCardID FLOAT,
    CreditCardApprovalCode VARCHAR(45),
    CurrencyRateID FLOAT,
    SubTotal VARCHAR(45),
    TaxAmt VARCHAR(45),
    Freight VARCHAR(45),
    TotalDue VARCHAR(45),
    Comment FLOAT,
    rowguid VARCHAR(45),
    ModifiedDate DATETIME
);

CREATE TABLE IF NOT EXISTS ROX.Customer
(
    CustomerID INT PRIMARY KEY NOT NULL,
    PersonID INT,
    StoreID FLOAT,
    TerritoryID INT,
    AccountNumber VARCHAR(45),
    rowguid VARCHAR(45),
    ModifiedDate DATETIME
);

CREATE TABLE IF NOT EXISTS ROX.SalesOrderDetail
(
    SalesOrderID INT PRIMARY KEY NOT NULL,
    SalesOrderDetailID INT,
    CarrierTrackingNumber VARCHAR(45),
    OrderQty INT,
    ProductID INT,
    SpecialOfferID INT,
    UnitPrice VARCHAR(45),
    UnitPriceDiscount VARCHAR(45),
    LineTotal FLOAT,
    rowguid VARCHAR(45),
    ModifiedDate DATETIME
);

CREATE INDEX idx_productID ON ROX.Product (ProductID);
CREATE INDEX idx_customerID ON ROX.Customer (CustomerID);
CREATE INDEX idx_businessentityID ON ROX.Person (BusinessEntityID);
CREATE INDEX idx_salesorderID ON ROX.SalesOrderHeader (SalesOrderID);
CREATE INDEX idx_specialofferID ON ROX.SalesOrderDetail (SpecialOfferID);

ALTER TABLE ROX.SpecialOfferProduct
    ADD FOREIGN KEY (ProductID)
    REFERENCES ROX.Product (ProductID)
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE ROX.SalesOrderHeader
    ADD FOREIGN KEY (CustomerID)
    REFERENCES ROX.Customer (CustomerID)
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE ROX.Customer
    ADD FOREIGN KEY (PersonID)
    REFERENCES ROX.Person (BusinessEntityID)
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE ROX.SalesOrderDetail
    ADD FOREIGN KEY (SalesOrderID)
    REFERENCES ROX.SalesOrderHeader (SalesOrderID)
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE ROX.SalesOrderDetail
    ADD FOREIGN KEY (SpecialOfferID)
    REFERENCES ROX.SpecialOfferProduct (SpecialOfferID)
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
