-- Query 1
WITH temp AS (
	SELECT DISTINCT SalesOrderID as id,
		COUNT(SalesOrderID) as contagem
    FROM ROX.SalesOrderDetail
    GROUP BY SalesOrderID
)

SELECT * FROM temp
	WHERE contagem >= 3;

-- Query 2
SELECT prod.Name, prod.DaysToManufacture, detail.OrderQty
	FROM ROX.Product as prod
    INNER JOIN ROX.SalesOrderDetail as detail
		ON prod.ProductID = detail.ProductID
    INNER JOIN ROX.SpecialOfferProduct as spec
		ON spec.ProductID = prod.ProductID
	ORDER BY detail.OrderQty desc;

-- Query 3
SELECT p.FirstName, p.LastName, COUNT(h.SalesOrderID)
	FROM ROX.Person as p
    INNER JOIN ROX.Customer as c
		ON p.BusinessEntityID = c.PersonID
	INNER JOIN ROX.SalesOrderHeader as h
		ON c.CustomerID = h.CustomerID
	GROUP BY p.FirstName, p.LastName;

-- Query 4
SELECT s.ProductID, h.OrderDate, SUM(s.OrderQty)
	FROM ROX.SalesOrderHeader AS h
	INNER JOIN ROX.SalesOrderDetail AS s 
    ON h.SalesOrderID = s.SalesOrderID
	INNER JOIN ROX.Product AS p 
    ON s.ProductID = p.ProductID
	GROUP BY s.ProductID, h.OrderDate;

-- Query 5
SELECT SalesOrderID, OrderDate, TotalDue
FROM ROX.SalesOrderHeader
    WHERE YEAR(OrderDate) = 2011
    AND MONTH(OrderDate) = 9
    AND TotalDue > 1000
    ORDER BY TotalDue DESC;
